///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/01/2021
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <unistd.h>
#include <array>
#include <list>


#include "animal.hpp"

#define FARM_SIZE 30


using namespace std;
using namespace animalfarm;


int main() {

// Set seed for rand() as current time
srand (time(0));

// Create and fill std::array with random animals
array<Animal*, FARM_SIZE> animalArray;
animalArray.fill( NULL );
for (int i = 0 ; i < FARM_SIZE ; i++ ) {
   animalArray[i] = AnimalFactory::getRandomAnimal();
}

// Print std::array information
cout << std::boolalpha;
cout << endl << "Array of Animals: " << endl;
cout << "   Is it empty: " << animalArray.empty() << endl;
cout << "   Number of elements: " << animalArray.size() << endl;
cout << "   Max size: " << animalArray.max_size() << endl;

// Let each animal speak
for( Animal* animal : animalArray ) {
   cout << animal -> speak() << endl;
}

// Delete animals;
for( Animal* animal : animalArray ) {
   delete animal;
}


// Create and fill std::list with random animals
list<Animal*> animalList;
for (int i = 0 ; i < 25 ; i++ ) {
   animalList.push_front(AnimalFactory::getRandomAnimal());
}

// Print std::list information
cout << std::boolalpha;
cout << endl << "List of Animals: " << endl;
cout << "   Is it empty: " << animalList.empty() << endl;
cout << "   Number of elements: " << animalList.size() << endl;
cout << "   Max size: " << animalList.max_size() << endl;

// Let each animal speak
for( Animal* animal : animalList ) {
   cout << animal -> speak() << endl;
}

// Delete animals;
for( Animal* animal : animalList ) {
   delete animal;
}



// Testing Class Factory by generating 25 random animals
/*
for( int i = 0 ; i < 25 ; i++ ) {
   Animal* a = AnimalFactory::getRandomAnimal();
   cout << a -> speak() << endl;
   delete a;
}
*/


// Testing random attribute generators
/*
while (1) {
   cout << "Name : " << Animal::getRandomName() << endl;
   cout << "Gender : " << Animal::getRandomGender() << endl;
   cout << "Color : " << Animal::getRandomColor() << endl;
   cout << "Bool : " << Animal::getRandomBool() << endl;
   cout << "Weight : " << Animal::getRandomWeight( 10.5, 83.6 ) << endl;
   cout << endl;
   sleep(1);
}
*/

return 0;
}
