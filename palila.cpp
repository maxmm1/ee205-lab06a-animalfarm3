///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Max Mochzuki <maxmm@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string newFound, enum Color newColor, enum Gender newGender ) {
        gender = newGender;
        species = "Loxioides bailleui";
        featherColor = newColor;
        isMigratory = false;
	whereFound = newFound;
}


const string Palila::speak() {
        return string( "Tweet" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Palila::printInfo() {
	cout << "Palila" << endl;
	cout << "   Where Found = [" << whereFound << "]" << endl;
	Bird::printInfo();
}

} // namespace animalfarm

