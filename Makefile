###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Max Mochizuki  <maxmm@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   02/26/2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

fish.o: fish.cpp fish.hpp animal.hpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

bird.o: bird.cpp bird.hpp animal.hpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp


main: main.cpp *.hpp main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	g++ -o main main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	
clean:
	rm -f *.o main
