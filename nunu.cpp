///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newNative, enum Color newColor, enum Gender newGender ) {
        gender = newGender;
        species = "Fistularia chinensis";
        scaleColor = newColor;
        favoriteTemperature = 80.6;
	isNative = newNative;
}


const string Nunu::speak() {
        return string( "Bubble bubble" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nunu::printInfo() {
        cout << "Nunu" << endl;
	cout << "   Is Native = [" << std::boolalpha << isNative << "]" << endl;
        Fish::printInfo();
}

} // namespace animalfarm

