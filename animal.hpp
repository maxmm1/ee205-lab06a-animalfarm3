///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string>

#pragma once

#define RANDOM_NAME     Animal::getRandomName()
#define RANDOM_COLOR    Animal::getRandomColor()
#define RANDOM_GENDER   Animal::getRandomGender()
#define RANDOM_BOOL     Animal::getRandomBool()
#define RANDOM_WEIGHT   Animal::getRandomWeight( rand(), rand() )



using namespace std;

namespace animalfarm {

// Create enums for Gender and Color
enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { RED, ORANGE, YELLOW, GREEN, BLUE, WHITE, BLACK, BROWN, SILVER };  /// @todo Add more colors

// Create animal class
class Animal {
public:
	enum Gender gender;
	string species;

	virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

	static const         string getRandomName();
	   static int        randLowercase();
	   static int        randUppercase();

	static const Gender	getRandomGender();
	
	static const Color	getRandomColor();
	
	static const bool    getRandomBool();
	
	static const float	getRandomWeight( const float from, const float to );

   Animal();
   ~Animal();

};


class AnimalFactory {
public:
   static Animal* getRandomAnimal();

};


} // namespace animalfarm
